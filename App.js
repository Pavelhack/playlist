"use strict";

import React  from 'react';
import ReactDOM from 'react-dom';
import './components/index.css';
import Playlist from './components/playlist.js' ;

let list = "Playlist";
let itemArr = require("./array.json");
let options = require("./option.json");

    ReactDOM.render(
      < Playlist
          list = {list}
          item = {itemArr}
          arrOfOptions = {options}
        />
        
     , document.getElementById('playbord') 
    );
