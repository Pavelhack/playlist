import React from 'react';
import PropTypes from "prop-types";
import './index.css';

class Option extends React.Component{

   static propTypes = { 
     text: PropTypes.string.isRequired,
     code: PropTypes.number.isRequired,
     selectedCode: PropTypes.number, // может быть null, пока ни один ответ не выбран
     cbSelected: PropTypes.func.isRequired,
    };

   
  
   answerClicked = (EO) => {
     if(EO.target)
    this.props.cbSelected(this.props.code);
  }
   

   render(){
       console.log(this.props.selectedCode)
       console.log(this.props.code)
       return(
          <div>
            <label className='label'>
                <input type='radio' value={this.props.text}
                  checked={this.props.selectedCode==this.props.code}
                  onClick={this.answerClicked}
                />
                <span>{this.props.text}</span>
            </label>
          </div> 
   
       )      
 }
       
 };

 export default  Option;

  