import React, { Fragment } from 'react';
import PropTypes from "prop-types"
import './index.css';
import Track  from './track.js'; 
import Option from './option.js';
import Num from './num.js';
import Performance from "./performance.js"; 

class Playlist extends React.Component{

  static propTypes  = {
   list : PropTypes.string,
   item : PropTypes.array,
   arrOfOptions : PropTypes.arrayOf(
    PropTypes.shape({
      code : PropTypes.number,
      text : PropTypes.string,
    })
   )
  };

  static defaultProps = { 
    nathing: "Here is empty" 
  };

  state = {
   playlist : this.props.list,
   array : this.props.item,
   options : this.props.arrOfOptions,
   selectedCode: null,
   
  };

  answerSelected = (code) => {
    console.log('выбран ответ с кодом '+code);
    this.setState( {selectedCode:code} );
  }

 

    render() {
   let x = this.props.arrOfOptions.map(v => <Option code = {v.code} text = {v.text}
              selectedCode = {this.state.selectedCode}
              cbSelected = {this.answerSelected}
        />
        )
        console.log(x)

        return( 
            
           <Fragment>
             <div className = {"header"}>
              <h1>{"Playlist"}</h1>
               <div className = "block">
                  <div className = "label"> 
                 {x}
                  </div>
                  <div className = {"blockTrek"}>
                      <div className = {"numberTrack"}>
                        <Num array = {this.state.array} flag = {this.state.flagNum}/>
                      </div>
                      <div className = {"nameTrack"}>
                        <Track array = {this.state.playlist} flag = {this.state.flagTrack}/>
                      </div>
                      <div className = {"performer"}>
                        <Performance array = {this.state.playlist} flag = {this.state.flagPerformance} />
                      </div>
                  </div>
               </div>
            </div>
         </Fragment>
      )
    }
};

export default Playlist;

{/* <div className = "DivInputRadio" onClick = {() => this.funChoiceNum(true)}>
                    <input type ='checkbox' checked = { this.state.flagNum}/>
                       <h3>{'on number'}</h3>
                  </div>
                  <div className = "DivInputRadio" onClick = {() => this.funNewProd(true)}>
                    <input type ='checkbox' checked = { this.state.flagTrack}/>
                       <h3>{'on name of the track'}</h3>
                  </div>
                  <div className = "DivInputRadio" onClick = {() => this.funNewProd(true)}>
                    <input type ='checkbox' checked = { this.state.flagPerformance}/>
                       <h3>{'on name of the performer'}</h3>
                  </div> */}